cmake_minimum_required(VERSION 2.8)
project( TemplateMatching )
find_package( OpenCV REQUIRED )
#set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
#add_executable( TemplateMatch src/image_processing.cpp )
#target_link_libraries( TemplateMatch ${OpenCV_LIBS} )

#add_executable( CircleDetect src/circle_detect.cpp )
#target_link_libraries( CircleDetect ${OpenCV_LIBS} )

add_executable( RecognizeCommand src/shape_detect.cpp )
target_link_libraries( RecognizeCommand ${OpenCV_LIBS} )
