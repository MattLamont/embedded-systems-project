			Programming Assignment 1

Class: Embedded Systems Programming
Group 6: 
April Hudspeth
Matthew Lamont
Jason Minor
Jennifer Haigh
Micheal Richards

About the Program
-----------------
This program will connect an Arduino Pro Mini to a computer via Bluetooth and will output readings from an ultrasonic sensor using the Arduino IDE serial monitor. 


Installation
------------
Arduino IDE: https://www.arduino.cc/en/Main/Software
FTDI Drivers: http://www.ftdichip.com/FTDrivers.htm


Hardware
--------
Arduino Pro Mini
FTDI 
Ultrasonic Sensor
Bluetooth Module
Breadboard
Connecting cables
Mini USB cable
Computer with USB port and Bluetooth capability

Setup and Running the Program
-----------------------------
1. Install software and drivers on computer
2. Hook Arduino Mini Pro on breadboard
3. Connect FTDI to Arduino Pro Mini (aligning up the pins correctly)
4. Connect Ultrasonic Sensor to Arduino Pro using connector cables
   Vcc  -> Vcc
   Trig -> Digital pin 8
   Echo -> Digital pin 7
   GND  -> GND
5. Plug mini USB into FTDI and the other end to computer's USB port
6. Load the program ArduinoBluetooth.ino source code included in this folder into a new Arduino sketch. 
7. Select the correct USB port via Tools -> Port
8. Upload the program to the Arduino.
9. Connect the Bluetooth module to the Arduino using connector cables
   Vcc -> Vcc
   RXD -> TXO
   TXD -> RXI
   GND -> GND
10. Pair the Bluetooth device on the computer
11. Go back to the Arduino IDE and change the Port to the new Bluetooth port
12. View the data coming in by going to Tools -> Serial Monitor
13. A new window should pop up with the Bluetooth port displayed at the top. 
