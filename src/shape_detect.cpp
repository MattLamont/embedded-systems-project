#include <cv.h>
#include <highgui.h>
#include <iostream>
#include <vector>
#include <cmath>
#include <algorithm>

using namespace std;

float lineDistance( CvPoint* point1 , CvPoint* point2 )
{
    float x = abs( point1->x - point2->x );
    float y = abs( point1->y - point2->y );

    float dist = pow( x , 2 ) + pow( y , 2 );
    dist = sqrt( dist );

    return dist;
}


bool checkShapeRatioSquare( std::vector< CvPoint* > point_vector )
{
    CvPoint* point1 = point_vector[0];
    CvPoint* point2 = point_vector[1];
    CvPoint* point3 = point_vector[2];
    CvPoint* point4 = point_vector[3];

    float line1 = lineDistance( point1 , point2 );
    float line2 = lineDistance( point2 , point3 );
    float line3 = lineDistance( point3 , point4 );
    float line4 = lineDistance( point4 , point1 );
    float ratio1;
    float ratio2;

    if( line1 > line2 )
    {
        ratio1 = line1 / line2;
    }
    else
    {
        ratio1 = line2 / line1;
    }

    if( line3 > line4 )
    {
        ratio2 = line3 / line4;
    }
    else
    {
        ratio2 = line4 / line3;
    }

    if( ratio1 > 1.4 && ratio1 < 1.6 )
    {
        if( ratio2 > 1.4 && ratio2 < 1.6 )
        {
            return true;
        }
    }
    return false;

}

bool checkShapeRatioTriangle( std::vector< CvPoint* > point_vector )
{
    CvPoint* point1 = point_vector[0];
    CvPoint* point2 = point_vector[1];
    CvPoint* point3 = point_vector[2];

    float line1 = lineDistance( point1 , point2 );
    float line2 = lineDistance( point2 , point3 );
    float line3 = lineDistance( point3 , point1 );

    float float_array[] = { line1 , line2, line3 };
    float minimum = *std::min_element( float_array , float_array + 3 );
    float maximum = *std::max_element( float_array , float_array + 3 );

    float deviation = maximum - minimum;

    if( deviation < 100.0 )
    {
        return true;
    }

    return false;

}



bool checkShapeRatioPentagon( std::vector< CvPoint* > point_vector )
{
    CvPoint* point1 = point_vector[0];
    CvPoint* point2 = point_vector[1];
    CvPoint* point3 = point_vector[2];
    CvPoint* point4 = point_vector[3];
    CvPoint* point5 = point_vector[4];

    float line1 = lineDistance( point1 , point2 );
    float line2 = lineDistance( point2 , point3 );
    float line3 = lineDistance( point3 , point4 );
    float line4 = lineDistance( point4 , point5 );
    float line5 = lineDistance( point5 , point1 );

    float float_array[] = { line1 , line2, line3 , line4 , line5 };
    float minimum = *std::min_element( float_array , float_array + 5 );
    float maximum = *std::max_element( float_array , float_array + 5 );

    float deviation = maximum - minimum;

    if( deviation < 100.0 )
    {
        return true;
    }

    return false;

}


int main(int argc , char** argv)
{
    if( argc != 2 )
    {
        std::cerr << "Error: No image file path was provided.";
    }

    IplImage* img =  cvLoadImage(argv[1]);

    //show the original image
    //cvNamedWindow("Original" , CV_WINDOW_NORMAL);
    //cvShowImage("Original",img);

    //smooth the original image using Gaussian kernel to remove noise
    cvSmooth(img, img, CV_GAUSSIAN,3,3);

    //converting the original image into grayscale
    IplImage* imgGrayScale = cvCreateImage(cvGetSize(img), 8, 1);
    cvCvtColor(img,imgGrayScale,CV_BGR2GRAY);

    //cvNamedWindow("GrayScale Image" , CV_WINDOW_NORMAL);
    //cvShowImage("GrayScale Image",imgGrayScale);

    //thresholding the grayscale image to get better results
    cvThreshold(imgGrayScale,imgGrayScale,100,255,CV_THRESH_BINARY_INV);

    //cvNamedWindow("Thresholded Image" , CV_WINDOW_NORMAL);
    //cvShowImage("Thresholded Image",imgGrayScale);

    CvSeq* contour;  //hold the pointer to a contour
    CvSeq* result;   //hold sequence of points of a contour
    CvMemStorage *storage = cvCreateMemStorage(0); //storage area for all contours

    //finding all contours in the image
    cvFindContours(imgGrayScale, storage, &contour, sizeof(CvContour), CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE, cvPoint(0,0));

    bool shape_found = false;

    //iterating through each contour
    while(contour)
    {

        //obtain a sequence of points of the countour, pointed by the variable 'countour'
        result = cvApproxPoly(contour, sizeof(CvContour), storage, CV_POLY_APPROX_DP, cvContourPerimeter(contour)*0.02, 0);

        //if there are 3 vertices  in the contour and the area of the triangle is more than 100 pixels
        if(result->total==4 && fabs(cvContourArea(result, CV_WHOLE_SEQ))>10000 )
        {
            std::vector< CvPoint* > point_vector;

            for( int i = 0 ; i < 4 ; i++ )
            {
                point_vector.push_back( (CvPoint*)cvGetSeqElem( result , i ));
            }

            if( checkShapeRatioSquare( point_vector ) )
            {
                cvLine(img, *point_vector[0] , *point_vector[1] , cvScalar(255,0,0),4);
                cvLine(img, *point_vector[1] , *point_vector[2] , cvScalar(255,0,0),4);
                cvLine(img, *point_vector[2] , *point_vector[3] , cvScalar(255,0,0),4);
                cvLine(img, *point_vector[3] , *point_vector[0] , cvScalar(255,0,0),4);

                std::cerr << "command1";
                shape_found = true;
                break;
            }
        }




        if(result->total==3 && fabs(cvContourArea(result, CV_WHOLE_SEQ))>10000 )
        {

            std::vector< CvPoint* > point_vector;

            for( int i = 0 ; i < 3 ; i++ )
            {
                point_vector.push_back( (CvPoint*)cvGetSeqElem( result , i ));
            }

            if( checkShapeRatioTriangle( point_vector ) )
            {
                cvLine(img, *point_vector[0] , *point_vector[1] , cvScalar(255,0,0),4);
                cvLine(img, *point_vector[1] , *point_vector[2] , cvScalar(255,0,0),4);
                cvLine(img, *point_vector[2] , *point_vector[0] , cvScalar(255,0,0),4);

                std::cerr << "command2";
                shape_found = true;
                break;
            }

        }



        if(result->total==5 && fabs(cvContourArea(result, CV_WHOLE_SEQ))>10000 )
        {

            std::vector< CvPoint* > point_vector;

            for( int i = 0 ; i < 5 ; i++ )
            {
                point_vector.push_back( (CvPoint*)cvGetSeqElem( result , i ));
            }

            if( checkShapeRatioPentagon( point_vector ) )
            {
                cvLine(img, *point_vector[0] , *point_vector[1] , cvScalar(255,0,0),4);
                cvLine(img, *point_vector[1] , *point_vector[2] , cvScalar(255,0,0),4);
                cvLine(img, *point_vector[2] , *point_vector[3] , cvScalar(255,0,0),4);
                cvLine(img, *point_vector[3] , *point_vector[4] , cvScalar(255,0,0),4);
                cvLine(img, *point_vector[4] , *point_vector[0] , cvScalar(255,0,0),4);

                std::cerr << "command3";
                shape_found = true;
                break;
            }

        }

        //obtain the next contour
        contour = contour->h_next;

    }

    if( shape_found == false )
    {
        std::cerr << "no_command";
    }


    /*
    //show the image in which identified shapes are marked
    cvNamedWindow("Tracked" , CV_WINDOW_NORMAL);
    cvShowImage("Tracked",img);

    cvWaitKey(0); //wait for a key press

    //cleaning up
    cvDestroyAllWindows();
    cvReleaseMemStorage(&storage);
    cvReleaseImage(&img);
    cvReleaseImage(&imgGrayScale);
    */

    return 0;
}















/*
#include <cv.h>
#include <highgui.h>
using namespace std;

int main(int argc , char* argv[] )
{

  IplImage* img =  cvLoadImage(argv[1]);

 //show the original image
 cvNamedWindow("Raw");
 cvShowImage("Raw",img);

  //converting the original image into grayscale
 IplImage* imgGrayScale = cvCreateImage(cvGetSize(img), 8, 1);
 cvCvtColor(img,imgGrayScale,CV_BGR2GRAY);

  //thresholding the grayscale image to get better results
 cvThreshold(imgGrayScale,imgGrayScale,128,255,CV_THRESH_BINARY);

 CvSeq* contours;  //hold the pointer to a contour in the memory block
 CvSeq* result;   //hold sequence of points of a contour
 CvMemStorage *storage = cvCreateMemStorage(0); //storage area for all contours

 //finding all contours in the image
 cvFindContours(imgGrayScale, storage, &contours, sizeof(CvContour), CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE, cvPoint(0,0));

 //iterating through each contour
 while(contours)
 {
     //obtain a sequence of points of contour, pointed by the variable 'contour'
     result = cvApproxPoly(contours, sizeof(CvContour), storage, CV_POLY_APPROX_DP, cvContourPerimeter(contours)*0.02, 0);

     //if there are 3  vertices  in the contour(It should be a triangle)
    if(result->total==3 )
     {
         //iterating through each point
         CvPoint *pt[3];
         for(int i=0;i<3;i++){
             pt[i] = (CvPoint*)cvGetSeqElem(result, i);
         }

         //drawing lines around the triangle
         cvLine(img, *pt[0], *pt[1], cvScalar(255,0,0),4);
         cvLine(img, *pt[1], *pt[2], cvScalar(255,0,0),4);
         cvLine(img, *pt[2], *pt[0], cvScalar(255,0,0),4);

     }

      //if there are 4 vertices in the contour(It should be a quadrilateral)
     else if(result->total==4 )
     {
         //iterating through each point
         CvPoint *pt[4];
         for(int i=0;i<4;i++){
             pt[i] = (CvPoint*)cvGetSeqElem(result, i);
         }

         //drawing lines around the quadrilateral
         cvLine(img, *pt[0], *pt[1], cvScalar(0,255,0),4);
         cvLine(img, *pt[1], *pt[2], cvScalar(0,255,0),4);
         cvLine(img, *pt[2], *pt[3], cvScalar(0,255,0),4);
         cvLine(img, *pt[3], *pt[0], cvScalar(0,255,0),4);
     }

   //if there are 7  vertices  in the contour(It should be a heptagon)
     else if(result->total ==7  )
     {
         //iterating through each point
         CvPoint *pt[7];
         for(int i=0;i<7;i++){
             pt[i] = (CvPoint*)cvGetSeqElem(result, i);
         }

         //drawing lines around the heptagon
         cvLine(img, *pt[0], *pt[1], cvScalar(0,0,255),4);
         cvLine(img, *pt[1], *pt[2], cvScalar(0,0,255),4);
         cvLine(img, *pt[2], *pt[3], cvScalar(0,0,255),4);
         cvLine(img, *pt[3], *pt[4], cvScalar(0,0,255),4);
         cvLine(img, *pt[4], *pt[5], cvScalar(0,0,255),4);
         cvLine(img, *pt[5], *pt[6], cvScalar(0,0,255),4);
         cvLine(img, *pt[6], *pt[0], cvScalar(0,0,255),4);
     }

      //obtain the next contour
     contours = contours->h_next;
 }

  //show the image in which identified shapes are marked
 cvNamedWindow("Tracked");
 cvShowImage("Tracked",img);

 cvWaitKey(0); //wait for a key press

  //cleaning up
 cvDestroyAllWindows();
 cvReleaseMemStorage(&storage);
 cvReleaseImage(&img);
 cvReleaseImage(&imgGrayScale);

  return 0;
}*/
